# Sơ Đồ Mạng LAN

![Alt text](<NODE MẠNG VĂN PHÚ-1.png>)

# Sơ Đồ Hệ Thống Mạng

![Alt text](<Sơ đồ đi dây Văn Phú.drawio-1.png>)

# Hệ Thống Âm Thanh

1. Âm Thanh

![Alt text](<Hệ Thống Âm Thanh Văn Phú.drawio-1.png>)

2. Bấm Số

![Alt text](<Bấm Số-1.png>)

# Sơ Đồ Panle

## Panle 1

| STT          | 1          | 2       | 3       | 4             | 5                 | 6        | 7       | 8       | 9       | 10          | 11      | 12        | 13        | 14        | 15       | 16       | 17      | 18         | 19         | 20             | 21       | 22      | 23      | 24       |
| :------------- | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ |
**Số Dây**  |99|124|125|97|102|126|122|100|96|98|121|101|91|82|95|89|86|85|82|84|81|80|83|103|
**Vị Trí** ||||Cam PH|||||Wifi PH|M.PH|||ĐT VP|M.Kho|Wifi VP|Máy Photo||M.QA|||M.Admin||||
**SW port** ||||Port 8-sw3|||||Port 3-sw3|Port 22-sw3|||Port 16-sw3|Port 14-sw3|Port 2-FW|Port 20-sw3||Port 15-sw3|||Port 13-sw3||||
**STT**      | **25**     | **26**  | **27**  | **28**        | **29**            | **30**   | **31**  | **32**  | **33**  | **34**      | **35**  | **36**    | **37**    | **38**    | **39**   | **40**   | **41**  | **42**     | **43**     | **44**         | **45**   | **46**  | **47**  | **48**   |
**Số Dây**   |32|39|38|115|15|16|106|3|105|17|4|103|104|9|8|11|10|12|13|90|7|6|5|14|
**Vị Trí** |Tivi PK1||||Wifi KVC|||Tivi KVC|||Cam KVC|||Cam PT3-CC|Cam PT3|||Tivi PT3|||M.PT3|||Wifi PT-CC|
**SW port**  |Port 11-sw3||||Port 2-sw2|||Port 12-sw2|||Port 7-sw2|||Port 11-sw1|Port 10-sw1|||Port 13-sw1|||Port 17-sw3|||Port 1-sw1|

## Panle 2

| STT          | 1          | 2       | 3       | 4             | 5                 | 6        | 7       | 8       | 9       | 10          | 11      | 12        | 13        | 14        | 15       | 16       | 17      | 18         | 19         | 20             | 21       | 22      | 23      | 24       |
| :------------- | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ |
**Số Dây**  |78|74|76|72|71|60|61|64|63|69|67|70|73|68|62|66|65|59|75|79|77|113|116|114|
**Vị Trí** |XWEB|Wifi Cửa Kho Lạnh||Wifi Cửa Bé Bú|Cam Ngoài Sảnh Sau|M.PK3|Tivi PT4||||Cam Cửa Phụ|Wifi TN Nhỏ|M.PT4||Cam PT4||Tivi PK3|Cam Cửa Kho Lạnh||Cam Kho Lạnh||||
**SW port** |Port 19-sw1|Port 1-sw3||Port 3-sw2|Port 4-sw3|Port 17-sw2|Port 16-sw1|Port 13-sw2||||Port 6-sw2|Port 3-sw1|Port 19-sw3||Port 10-sw2||Port 10-sw3|Port 8-sw1||Port 5-sw3||||
**STT**      | **25**     | **26**  | **27**  | **28**        | **29**            | **30**   | **31**  | **32**  | **33**  | **34**      | **35**  | **36**    | **37**    | **38**    | **39**   | **40**   | **41**  | **42**     | **43**     | **44**         | **45**   | **46**  | **47**  | **48**   |
**Số Dây**   |52|50|48|51|53|45|44|55|117|120|118|115|58|54|56|47|46|57|41|35|37|40|33|49|
**Vị Trí**   |Cam PT2|Tivi PT2|M.PT1|||Cam PT1|Tivi PT1||Wifi Căng Tin||MCC|||M.PT2|Cam TN Nhỏ|||M.TN Nhỏ|M.PK2||Tivi PK2|M.PK2|M.PK1||
**SW port**  |Port 7-sw3|Port 12-sw3|Port 19-sw2|||Port 11-sw2|Port 9-sw3||Port 2-sw3||Port 21-sw3|||Port 18-sw3|Port 5-sw2|||Port 18-sw2|Port 15-sw2||Port 12-sw1|Port 14-sw2|Port 17-sw1||

## Panle 3

| STT          | 1          | 2       | 3       | 4             | 5                 | 6        | 7       | 8       | 9       | 10          | 11      | 12        | 13        | 14        | 15       | 16       | 17      | 18         | 19         | 20             | 21       | 22      | 23      | 24       |
| :------------- | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ | :------------------ |
**Số Dây**  |21|36|34|108|27|109|111|127|26|2|1|20|30|107|18|22|110|29|19|23|25|28|24|31|
**Vị Trí** |||M.PK1||||||||Cam Ngoài Trời|Wifi LT-TN||Wifi PK1|Cam ST|||M.LT-TN 1|Cam Cửa Chính||M.LT-TN 3|M.LT-TN 2||Cam LT-TN|
**SW port** |||Port 18-sw1||||||||Port 9-sw2|Port 1-sw2||Port 2-sw1|Port 9-sw1|||Port 15-sw1|Port 7-sw2||Port 14-sw1|Port 16-sw1||port 5-sw1|
**STT**      | **25**     | **26**  | **27**  | **28**        | **29**            | **30**   | **31**  | **32**  | **33**  | **34**      | **35**  | **36**    | **37**    | **38**    | **39**   | **40**   | **41**  | **42**     | **43**     | **44**         | **45**   | **46**  | **47**  | **48**   |
**Số Dây**   |||||||||||||||||||||94|93|42|43|
**Vị Trí**   |||||||||||||||||||||Cam VP||Cam Cửa Sever|Cam Sever|
**SW port**  |||||||||||||||||||||Port 8-sw2||Port 6-sw1|Port 6-sw3|

# IP Ranger & Vlan

## Internet

| STT | ISP          | IP WAN         |
| --- | -----------  | -------------  |
| 1   | VIETTEL (27) | 27.72.97.169   |
| 2   | VIETTEL (28) | 27.72.104.117  |

## IP Ranger

| Vlan Name     | IP Ranger        | Port on SW-1  | Port on SW-2 | Port on SW-3 |
| ------------- | ---------------- | ------------  |--------------|--------------|
| Core          | 172.17.140.0/24  | 1-3           | 1-3          | 1-3          |
| Camera & Tivi | 192.168.155.0/24 | 4-13          | 4-13         | 4-12         |
| Vlan5         | 10.14.135.0/24   | 14-19         | 14-19        | 13-22        |
| Wifi          | 172.15.10.0/24   |               |              |              |
| Wifi- Public  | 192.168.100.0/22 |               |              |              |

# Wifi & Camer

## Wifi

 | STT | Name             | IP            |
 | --- | ---------------- | ------------- |
 | 1   | Văn Phòng        | 172.17.140.50 |
 | 2   | PT-CC            | 172.17.140.51 |
 | 3   | Khu Vui Chơi     | 172.17.140.52 |
 | 4   | TN Nhỏ           | 172.17.140.53 |
 | 5   | Lễ Tân           | 172.17.140.54 |
 | 6   | Phòng Khám 1     | 172.17.140.55 |
 | 7   | Cửa Phòng Bé Bú  | 172.17.140.56 |
 | 8   | Phòng Họp        | 172.17.140.57 |
 | 9   | Căng Tin         | 172.17.140.58 |
 | 10  | Cửa Kho Lạnh     | 172.17.140.59 |

## Camer

 | STT | Name             | IP Address     |
 | --- | --------------   | -------------- |
 | 1   | Thu Ngân- Nhỏ    | 192.168.155.20 |
 | 2   | Cửa Chính        | 192.168.155.21 |
 | 3   | Khu Vui Chơi     | 192.168.155.22 |
 | 4   | Cửa Sever        | 192.168.155.23 |
 | 5   | Sau Tiêm         | 192.168.155.24 |
 | 6   | Cửa Kho Lạnh     | 192.168.155.25 |
 | 7   | Lễ Tân- Thu ngân | 192.168.155.26 |
 | 8   | Cửa Phụ          | 192.168.155.27 |
 | 9   | Ngoài Sảnh Sau   | 192.168.155.28 |
 | 10  | Văn Phòng        | 192.168.155.29 |
 | 11  | Kho Lạnh         | 192.168.155.30 |
 | 12  | Phòng Sever      | 192.168.155.31 |
 | 13  | Ngoài Sảnh Trước | 192.168.155.32 |
 | 14  | PT3              | 192.168.155.50 |
 | 15  | PT3- CC          | 192.168.155.51 |
 | 16  | PT4              | 192.168.155.52 |
 | 17  | PH1              | 192.168.155.53 |
 | 18  | PH               | 192.168.155.54 |
 | 19  | PT2              | 192.168.155.55 |
 | 20  | Nas              | 192.168.155.5  |

# EXT IP Phone

| STT | Extension | Password              | IP           | Vị Trí                |
| --- | --------- | --------------------- | ------------ | --------------------- |
| 1   | 40301     |    .P$5veFS.B         | 10.14.135.51 | LT                    |
| 2   | 40302     |    i0oeZ0rIbC         | 10.14.135.65 | VP                    |
| 3   | 40303     |    E.$U9gk9oF         | 10.14.135.52 | PT1                   |
| 4   | 40304     |    nY8A!z!PrV         | 10.14.135.53 | PT2                   |
| 5   | 40305     |    me%!6huVSs         | 10.14.135.50 | PK3-01                |
| 6   | 40306     |    o6k.oG5oYq         | 10.14.135.70 | PT-CC                 |
| 7   | 40307     |    ..h4D!Xhun         |              |                       |
| 8   | 40308     |    rI.b9q^Bb0         |              |                       |
| 9   | 40309     |    !!.u2iJnnB         |              |                       |
| 10  | 40310     |    s..bvjFLbI         |              |                       |
| 11  | 1676      | VxiPfiqjtBIE9tvTdWUPa |              | Oncall Bs             |
| 12  | 1677      | Q4vYur4vsvavIr5MlYNdP | 10.14.135.61 | Oncall cskh (PK3-02)  |

# Switch Config

## Switch 01

```
conf ter
interface range GigabitEthernet1/0/23-24
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
exit
vlan 2
name MGMT
vlan 100
name WIFI
vlan 101
name WIFI-PUBLIC
vlan 99
name CAMERA
vlan 5
name PK-PT
vlan 201
name ISP1
vlan 202
name ISP2
!
!
port-channel load-balance src-dst-ip
hostname SW-92001
username admin privilege 15 password VT@$14
aaa new-model
aaa authentication login default local
aaa authorization exec default local 
aaa authorization network default local 
aaa session-id common
ip domain-name vanphu.vnvc.vn
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 ip address 172.17.140.11 255.255.255.0
!
ip default-gateway 172.17.140.1

ip http server
ip http secure-server
!
ip ssh version 2
!
line con 0
line vty 5 15
!
interface range GigabitEthernet1/0/1-3
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
!
interface range GigabitEthernet1/0/4-13
switchport mode access
switchport access vlan 99
!
interface range GigabitEthernet1/0/14-19
switchport mode access
switchport access vlan 5
!
interface range GigabitEthernet1/0/20-22
switchport mode access
switchport access vlan 201
!
end
config ter
ip default-gateway 172.17.140.1
end
copy ru sta

```

## Switch 02

```
conf ter
interface range GigabitEthernet1/0/23-24
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
exit
vlan 2
name MGMT
vlan 100
name WIFI
vlan 101
name WIFI-PUBLIC
vlan 99
name CAMERA
vlan 5
name PK-PT
vlan 201
name ISP1
vlan 202
name ISP2
!
!
port-channel load-balance src-dst-ip
hostname SW-92002
username admin privilege 15 password VT@$14
aaa new-model
aaa authentication login default local
aaa authorization exec default local 
aaa authorization network default local 
aaa session-id common
ip domain-name vanphu.vnvc.vn
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 ip address 172.17.140.12 255.255.255.0
!
ip default-gateway 172.17.140.1

ip http server
ip http secure-server
!
ip ssh version 2
!
line con 0
line vty 5 15
!
interface range GigabitEthernet1/0/1-3
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
!
interface range GigabitEthernet1/0/4-13
switchport mode access
switchport access vlan 99
!
interface range GigabitEthernet1/0/14-19
switchport mode access
switchport access vlan 5
!
interface range GigabitEthernet1/0/20-22
switchport mode access
switchport access vlan 202
!
end
config ter
ip default-gateway 172.17.140.1
end
copy ru sta

```

## Switch 03

```
conf ter
interface range GigabitEthernet1/0/23-24
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
exit
vlan 2
name MGMT
vlan 100
name WIFI
vlan 101
name WIFI-PUBLIC
vlan 99
name CAMERA
vlan 5
name PK-PT
vlan 201
name ISP1
vlan 202
name ISP2
!
!
port-channel load-balance src-dst-ip
hostname SW-92003
username admin privilege 15 password VT@$14
aaa new-model
aaa authentication login default local
aaa authorization exec default local 
aaa authorization network default local 
aaa session-id common
ip domain-name vanphu.vnvc.vn
interface Vlan1
 no ip address
 shutdown
!
interface Vlan2
 ip address 172.17.140.13 255.255.255.0
!
ip default-gateway 172.17.140.1

ip http server
ip http secure-server
!
ip ssh version 2
!
line con 0
line vty 5 15
!
interface range GigabitEthernet1/0/1-3
switchport mode trunk
switchport trunk native vlan 2
switchport trunk allow vlan all
!
interface range GigabitEthernet1/0/4-12
switchport mode access
switchport access vlan 99
!
interface range GigabitEthernet1/0/13-22
switchport mode access
switchport access vlan 5
!
end
config ter
ip default-gateway 172.17.140.1
end
copy ru sta

```
